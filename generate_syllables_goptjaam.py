"""
This is the script used to convert the file guangyun0704/Kuankhiunn0704-semicolon.txt to
syllables-goptjaam.csv, which retains only syllable information relevant to Goptjaam.
"""
import pandas as pd
import os


# Define functions used to test for Han characters

# See https://en.wikipedia.org/wiki/CJK_Unified_Ideographs_(Unicode_block)#References
# Only actual Han characters; no punctuation, radicals, etc.
INTERVALS_CJK = [
    ('4e00', '9fff'),
    ('3400', '4dbf'),
    ('20000', '2ebef'),
    ('30000', '323af'),
]


def remove_non_han_chars(string: str) -> str:
    return ''.join(char for char in string if is_han_char(char))


def is_han_char(char: str) -> bool:
    return any(int(interval[0], 16) <= ord(char[0]) <= int(interval[1], 16) for interval in INTERVALS_CJK)


# Load data
columns_old = [
    '舊版序號', '序號', '反切', '字', '字數', '校驗表記', '韻目', '韻中序號',
    '聲', '呼', '等', '韻', '調',
    'polyhedron羅馬字', '有馬羅馬字', '舊版備註', '復校備註', '小韻韻目歸屬說明', '異體字', 'IDS'
]
syllables = pd.read_csv(
    os.path.join(os.path.dirname(__file__), 'guangyun0704', 'Kuankhiunn0704-semicolon.txt'),
    skiprows=range(27),
    header=None,
    sep=';',
    names=columns_old,
    index_col='序號')
# Note that <final>B == 重紐 division 3; <final>A == 重紐 division 4

# Delete extra spaces (typos in original document): under column 聲, rows 2842 and 2886
syllables.loc[2842, '聲'] = syllables.loc[2842, '聲'].strip()
syllables.loc[2886, '聲'] = syllables.loc[2886, '聲'].strip()

# Delete rows where the number of characters is 0 or nan
syllables = syllables[(syllables['字數'] != 0) & (~syllables['字數'].isna())]
# Luckily, this results in 3877 syllables, which is a prime number!
# The "extra syllables" left have 序號 4002 (日;開;三;麻;去), 4003 (溪;開;三;侵B;去), and 4004 (清;合;一;桓;平)

# Renumber syllables such that they are \in [0, 3877), and order by new index:
# - 4004 (清;合;一;桓;平) \mapsto 0
# - 4002 (日;開;三;麻;去) \mapsto 3875
# - 4003 (溪;開;三;侵B;去) \mapsto 3876
# This order was chosen because in the 廣韻, (桓;平) comes before (麻;去), which is before (侵B;去)
syllables = syllables.rename(index={4004: 0, 4002: 3875, 4003: 3876}).sort_index()

# Append alternate characters (syllables['異體字']) to characters, and remove non-Han characters
for i in range(len(syllables)):
    chars_alt_han = remove_non_han_chars(str(syllables.loc[i, '異體字']))
    chars_han = remove_non_han_chars(str(syllables.loc[i, '字']))
    syllables.loc[i, '字'] = chars_han + chars_alt_han


# Remove duplicate characters; this takes 2 passes:
# Note that the first character of each entry of syllables['字'] is the character the syllable maps to
chars_first_unique = ''
chars_ambiguous = ''

# 1: collect the first character from each syllable.
# This is done in order of increasing number of characters in the syllable, then in increasing index number
print('Ambiguous characters: (# of chars in syllable, character, index)')
n_char_max = max([len(chars) for chars in syllables['字']])
for n_char in range(1, n_char_max + 1):  # Increasing in number of characters in the syllable
    for i in range(len(syllables)):  # Then increasing in index number
        if len(syllables.loc[i, '字']) == n_char:
            for j, char in enumerate(syllables.loc[i, '字']):
                if char not in chars_first_unique:
                    # Move only the new syllable to the front, and doesn't move all previous characters to the back
                    syllables.loc[i, '字'] = char + syllables.loc[i, '字'][:j] + syllables.loc[i, '字'][j + 1:]
                    chars_first_unique += char
                    break
            else:
                chars_ambiguous += syllables.loc[i, '字']
                print(f"{n_char}, {syllables.loc[i, '字']}, {i}")

# Ambiguous characters:
# 1 char in syllable: 㟅 𡰝 㱡 能 奻 扮 些 迎 凝 殑 出 𪗨 茁 啜 縛
# 2 chars in syllable: 蘬 䂳 絮 䫡 䵵


# 2: Manually resolve some ambiguous characters: characters that are in the first characters of multiple syllables
# Some of these can be disambiguated by assigning later (non-first) characters to the syllables
chars_disambiguated = '蘬䂳絮'
RESOLUTION_CHAR_AMBIGUITY = {
    # Case 蘬:
    # 183: 巋蘬 -> 巋蘬
    # 225: 蘬𧢦 -> 𧢦蘬
    # 1285: 巋蘬 -> 蘬巋
    183: '巋蘬',
    225: '𧢦蘬',
    1285: '蘬巋',

    # Case 䂳:
    # 762: 脞㛗 -> 㛗脞
    # 1762: 脞䂳
    # 1795: 䂳
    762: '㛗脞',

    # Case 絮:
    # 1335: 女籹 -> 籹女
    # 2311: 女絮
    # 2304: 絮
    1335: '籹女',

    # Case 䫡 unresolvable:
    # 2080: 䫡
    # 3121: 𪉦
    # 3166: 𪉦䫡

    # Case 䵵 unresolvable:
    # 3535: 㔍
    # 3459: 䵵㔍
    # 3527: 茁䵵
    # 3318, 3445: 茁
}
for index, char in RESOLUTION_CHAR_AMBIGUITY.items():
    syllables.loc[index, '字'] = char

# Remove disambiguated characters from chars_ambiguous
chars_ambiguous = ''.join(char_ for char_ in chars_ambiguous if char_ not in chars_disambiguated)
print(f'\nStill ambiguous chars: {chars_ambiguous}')


# Retain only relevant columns (initial, openness, division, rime, tone, characters)
syllables = syllables[['聲', '呼', '等', '韻', '調', '字']]


# Replace characters in syllables 0, 3876, and 3875
syllables.loc[0, '字'] = '〇'  # Zero circle
syllables.loc[3876, '字'] = '△、'  # Relative pronoun triangle; for ease of typing, also denoted by serial comma '、'
syllables.loc[3875, '字'] = '□'  # Character not found square


#################
# Goptjaam v2.0 #
#################
# Changelog:
# - add Transcription (Baxter–Sagart) and Orthography, IPA (Sino-Dutch) to dataframe syllables
# - separate column '字' into 'Character' (first character) and 'Homophones' (the rest)
# - ensured that Index and (Character, Orthography) are bijective

# Import csvs containing Baxter–Sagart and Sino-Dutch initials and finals
# This is the Baxter–Sagart 2014 version; the Wikipedia article on it is the Baxter 1992 version
baxter_sagart_initials = pd.read_csv(os.path.join(os.path.dirname(__file__), 'data', 'baxter_sagart_initials.csv'),
                                     index_col='聲').to_dict()['Initial']
baxter_sagart_rimes = pd.read_csv(os.path.join(os.path.dirname(__file__), 'data', 'baxter_sagart_rimes.csv'),
                                  index_col='韻').to_dict()['Rime']
sino_dutch_initials = pd.read_csv(os.path.join(os.path.dirname(__file__), 'data', 'sino_dutch_initials.csv'))
sino_dutch_finals = pd.read_csv(os.path.join(os.path.dirname(__file__), 'data', 'sino_dutch_finals.csv'))


# Define functions to map from (initial, openness, division, rime, tone) to
def syllable_to_baxter_sagart(row, return_separate=False):
    # print(row)
    initial, openness, division, rime, tone = row[['聲', '呼', '等', '韻', '調']]

    initial_bs = baxter_sagart_initials[initial]
    final_bs = baxter_sagart_rimes[rime[0]]

    # We modify final_bs to make it the actual Baxter–Sagart final
    # Uncompress Baxter's notation: deal with division 3, division 3A (重紐 division 4), and closed medials
    if division == '三' and final_bs[0] not in ['j', 'i']:
        final_bs = 'j' + final_bs
    if division == '三' and rime[-1] == 'A':
        final_bs = 'j' + final_bs
    final_bs = final_bs.replace('jj', 'ji')
    if openness == '合':
        if final_bs.startswith('j'):
            final_bs = 'jw' + final_bs[1:]
        else:
            final_bs = 'w' + final_bs

    # Now we combine the initial and final
    # Special rules:

    # Implement rule that labial initial + wa/wan/wat -> labial initial + a/an/at
    # See Baxter–Sagart 2014:18; for 末, Qieyun suggests mat, while Guangyun says mwat
    if initial_bs in ['p', 'ph', 'b', 'm'] and final_bs in ['wa', 'wan', 'wat']:
        final_bs = final_bs[1:]

    # Implement rule that grave initial + iw/jeng/jweng -> grave initial + jiw/jieng/jwieng
    # See Baxter–Sagart 2014:19
    # Grave initial = labial, velar, or glottal (see Baxter–Sagart 2014:15)
    if initial_bs in ['p', 'ph', 'b', 'm', 'k', 'kh', 'g', 'ng', "'", 'x', 'h']:
        if final_bs == 'iw':
            final_bs = 'jiw'
        elif final_bs == 'jeng':
            final_bs = 'jieng'
        elif final_bs == 'jweng':
            final_bs = 'jwieng'

    # I'm not implementing the exception that 鏐 is ljiw not liw

    # Add tone
    if tone == '上':
        final_bs += 'X'
    elif tone == '去':
        final_bs += 'H'
    elif tone == '入':
        if final_bs.endswith('m'):
            final_bs = final_bs[:-1] + 'p'
        elif final_bs.endswith('n'):
            final_bs = final_bs[:-1] + 't'
        elif final_bs.endswith('ng'):
            final_bs = final_bs[:-2] + 'k'

    if not return_separate:
        # Implement orthography rule that for palatal initial + j, j redundant and so is not written
        # See Baxter–Sagart 2014:16
        if 'y' in initial_bs and final_bs[0] == 'j':
            final_bs = final_bs[1:]

        # Combine initial and final
        bs = initial_bs + final_bs

        return bs
    else:
        return initial_bs, final_bs


def syllable_to_sino_dutch(row) -> tuple[str, str]:
    initial, openness, division, rime, tone = row[['聲', '呼', '等', '韻', '調']]

    # Extract chongniu ('A', 'B', or '')
    if rime[-1] in ['A', 'B']:
        chongniu = rime[-1]
        rime = rime[:-1]
    else:
        chongniu = ''

    # Get initial from syllable description (yeah I could have used a dict but whatever)
    for i in range(len(sino_dutch_initials)):
        if initial in sino_dutch_initials.loc[i, '聲']:
            initial_sd = sino_dutch_initials.loc[i, 'Initial']
            initial_ipa_sd = sino_dutch_initials.loc[i, 'IPA']
            break
    else:
        raise NotImplementedError(f'Current implementation of Sino-Dutch initials doesn\'t include {initial}!')
    # Delete ∅
    if initial_sd == '∅':
        initial_sd = ''
    if initial_ipa_sd == '∅':
        initial_ipa_sd = ''

    # Get final from syllable description
    division_hindu_arabic = {'一': '1', '二': '2', '三': '3', '四': '4'}[division]
    division_chongniu = division_hindu_arabic + chongniu
    for i in range(len(sino_dutch_finals)):
        row_sd = sino_dutch_finals.loc[i]
        if rime in row_sd['韻'] and division_chongniu == row_sd['等'] and (pd.isna(row_sd['呼']) or openness in row_sd['呼']):
            final_sd = row_sd['Final']
            final_ipa_sd = row_sd['IPA']
            break
    else:
        raise NotImplementedError(
            f'Current implementation of Sino-Dutch finals doesn\'t include {rime},{division_chongniu},{openness}!\nrow:\n{row}')

    # Get Baxter–Sagart notation
    initial_bs, final_bs = syllable_to_baxter_sagart(row, return_separate=True)

    # Add tone
    if tone == '平':
        # Low tone; not marked
        pass
    elif tone == '上':
        # High tone; add acute accent marks to all vowel letters
        list_rising = [('a', 'á'), ('e', 'é'), ('ij', 'íj́'), ('i', 'í'), ('o', 'ó'), ('u', 'ú')]
        for vowel, acute in list_rising:
            final_sd = final_sd.replace(vowel, acute)
        initial_ipa_sd = "'" + initial_ipa_sd
    elif tone == '去':
        final_sd += 's'
        final_ipa_sd += 's'
    elif tone == '入':
        if final_bs.endswith('p'):
            final_sd = final_sd[:-1] + 'p'
            final_ipa_sd = final_ipa_sd[:-1] + 'p'
        elif final_bs.endswith('t'):
            final_sd = final_sd[:-1] + 't'
            final_ipa_sd = final_ipa_sd[:-1] + 't'
        elif final_bs.endswith('k'):
            final_sd = final_sd[:-2] + 'k'
            final_ipa_sd = final_ipa_sd[:-1] + 'k'

    # Conform to Dutch orthography regarding double vowel letters
    if len(final_sd) >= 2 and final_sd[-2:] in ['aa', 'áá', 'oo', 'óó']:
        final_sd = final_sd[:-1]

    # Combine initial and final
    sino_dutch = initial_sd + final_sd
    sino_dutch_ipa = initial_ipa_sd + final_ipa_sd

    # jj > j
    sino_dutch = sino_dutch.replace('jj', 'j')
    sino_dutch_ipa = sino_dutch_ipa.replace('jj', 'j')

    return sino_dutch, sino_dutch_ipa


# Add Baxter–Sagart transcription and Sino-Dutch orthography and IPA to syllables
# (yes I know that syllable_to_sino_dutch also calls syllable_to_baxter_sagart
# so the same code is run twice but whatever)
syllables['Transcription'] = syllables.apply(syllable_to_baxter_sagart, axis=1)
syllables['Orthography_IPA'] = syllables.apply(syllable_to_sino_dutch, axis=1)
syllables['Orthography'] = syllables.apply(lambda row: row['Orthography_IPA'][0], axis=1)
syllables['IPA'] = syllables.apply(lambda row: row['Orthography_IPA'][1], axis=1)


# Determine which characters to use as the only possible character for each syllable (reorder characters)
# Only reorder if an obviously more common/useful character than the initial character in column '字' exists
def reorder_to_front(string: str, character: str) -> str:
    # If character in string, then move character to start of string
    # Else, raises ValueError
    i_char = string.index(character)
    return string[i_char] + string[:i_char] + string[i_char + 1:]


syllables.at[1090, '字'] = reorder_to_front(syllables.at[1090, '字'], '三')  # Replace 弎
syllables.at[1933, '字'] = reorder_to_front(syllables.at[1933, '字'], '九')  # Replace 久
syllables.at[3599, '字'] = reorder_to_front(syllables.at[3599, '字'], '百')  # Replace 伯

syllables.at[406, '字'] = reorder_to_front(syllables.at[406, '字'], '人')  # Replace 仁
syllables.at[678, '字'] = reorder_to_front(syllables.at[678, '字'], '橋')  # Replace 喬
syllables.at[941, '字'] = reorder_to_front(syllables.at[941, '字'], '聽')  # Replace 汀
syllables.at[3000, '字'] = reorder_to_front(syllables.at[3000, '字'], '汀')  # Replace 聽
syllables.at[3361, '字'] = reorder_to_front(syllables.at[3361, '字'], '掘')  # Replace 𧤼

# For Goptjaam 1.0 compatibility
syllables.at[806, '字'] = reorder_to_front(syllables.at[806, '字'], '翔')  # Replace 詳
syllables.at[60, '字'] = reorder_to_front(syllables.at[60, '字'], '蜂')  # Replace 峯
syllables.at[215, '字'] = reorder_to_front(syllables.at[215, '字'], '飛')  # Replace 猆
syllables.at[990, '字'] = reorder_to_front(syllables.at[990, '字'], '猶')  # Replace 猷

syllables.at[692, '字'] = reorder_to_front(syllables.at[692, '字'], '貓')  # Replace 茅
syllables.at[12, '字'] = reorder_to_front(syllables.at[12, '字'], '熊')  # Replace 雄


# Split '字' into 'Character' and 'Homophones'
syllables['Character'] = syllables.apply(lambda row: row['字'][0], axis=1)
syllables['Homophones'] = syllables.apply(lambda row: row['字'][1:], axis=1)


# Test for bijectivity between Index and (Character, Orthography)
counts_character_orthography = syllables.value_counts(subset=['Character', 'Orthography'])
is_bijective = len(syllables) == len(counts_character_orthography)
print(f'\nBijectivity between Index and (Character, Orthography)?: {is_bijective}')
if not is_bijective:
    print(counts_character_orthography[counts_character_orthography > 1])
# 1 pair of duplicate (Character, Orthography) tuples: (𪗨, djit), indices 3312 and 3339

# Achieve bijectivity by replacing one of the 𪗨 (U+2A5E8) with a variant character 䶡 (U+4DA1) (not in Guangyun)
# See: https://glyphwiki.org/wiki/u2a5e8
print('\nDisambiguating by replacing with variant character…')
syllables.at[3339, 'Character'] = '䶡'

# Test again for bijectivity between Index and (Character, Orthography)
counts_character_orthography = syllables.value_counts(subset=['Character', 'Orthography'])
is_bijective = len(syllables) == len(counts_character_orthography)
print(f'\nBijectivity between Index and (Character, Orthography)?: {is_bijective}')
if not is_bijective:
    print(counts_character_orthography[counts_character_orthography > 1])


# Save syllables to file
syllables.to_csv(os.path.join(os.path.dirname(__file__), 'guangyun0704', 'syllables-goptjaam.csv'), sep=';',
                 columns=['聲', '呼', '等', '韻', '調',
                          'Character', 'Homophones', 'Transcription', 'Orthography', 'IPA'])
