import numpy as np
import pandas as pd
import os
import argparse
import warnings


def load_syllables_goptjaam() -> pd.DataFrame:
    return pd.read_csv(os.path.join(os.path.dirname(__file__), 'guangyun0704', 'syllables-goptjaam.csv'),
                       sep=';',
                       na_filter=False,  # As some entries are "nan", which are erroneously parsed as NaN by default
                       index_col='序號')


class MCSyllables:
    def __init__(self):
        self.syllables = load_syllables_goptjaam()
        self.p_syllables = len(self.syllables)  # = number of syllables = 3877

    ############
    # Encoding #
    ############

    def classical_chinese_to_indices_character(self, classical_chinese: str) -> list[int]:
        # Multiple indices may map to the same character
        df_match_character = self.syllables.query(f'Character == "{classical_chinese}"')
        return df_match_character.index.to_list()

    def classical_chinese_to_indices_homophone(self, classical_chinese: str) -> list[int]:
        # Multiple indices may map to the same character
        df_match_homophone = self.syllables[self.syllables['Homophones'].str.contains(classical_chinese)]
        return df_match_homophone.index.to_list()

    def index_to_character(self, index: int) -> str:
        # Assumes that index is a valid index of self.syllables
        assert 0 <= index < self.p_syllables
        return self.syllables.at[index, 'Character']

    def index_to_transcription(self, index: int) -> str:
        # Assumes that index is a valid index of self.syllables
        assert 0 <= index < self.p_syllables
        return self.syllables.at[index, 'Transcription']

    def index_to_orthography(self, index: int) -> str:
        # Assumes that index is a valid index of self.syllables
        assert 0 <= index < self.p_syllables
        return self.syllables.at[index, 'Orthography']

    def index_to_ipa(self, index: int) -> str:
        # Assumes that index is a valid index of self.syllables
        assert 0 <= index < self.p_syllables
        return self.syllables.at[index, 'IPA']

    ############
    # Decoding #
    ############

    def character_orthography_to_index(self, character: str, orthography: str) -> int:
        # Assumes that (character, orthography) is bijective with index
        df_match = self.syllables.query(f'(Character == "{character}") and (Orthography == "{orthography}")')
        if df_match.empty:
            raise ValueError(f'({character}, {orthography}) is not a valid Goptjaam (Character, Orthography) pair!')
        return df_match.index[0]

    #########
    # Other #
    #########

    def subdf_from_indices(self, indices: list[int]) -> pd.DataFrame:
        return self.syllables.loc[indices, :]


# Infinite self-inverse matrices

def generate_alternating_self_inverse_matrix(p: int, n: int, is_upper: bool) -> np.ndarray:
    # Check if p >= 3 and p odd
    if not (p >= 3 and p % 2 == 1):
        raise ValueError(f'p must be odd and ≥ 3, but is {p}')

    # Check if n is >= 1
    if not n >= 1:
        raise ValueError(f'n must be ≥ 1, but is {n}')

    is_upper_int = 1 if is_upper else 0

    # Construct matrix
    mat = np.diag((-1) ** np.arange(n))

    # Construct grid
    row = np.zeros(n, dtype='int64')
    product = 1
    j = is_upper_int
    while j < n:
        row[j] = product
        product = (2 * product) % p
        j += 2

    i = 1 - is_upper_int
    while i < n:
        mat[i] += row
        row = (2 * row) % p
        i += 2

    return mat


def generate_triangular_self_inverse_matrix(p: int, n: int) -> np.ndarray:
    # Check if p >= 3 and p odd
    if not (p >= 3 and p % 2 == 1):
        raise ValueError(f'p must be odd and ≥ 3, but is {p}')

    # Check if n is >= 1
    if not n >= 1:
        raise ValueError(f'n must be ≥ 1, but is {n}')

    # For algorithms 1, 2, 3:
    # # Construct matrix J (this is matrix D in the video)
    # mat_J = np.diag((-1) ** np.arange(n, dtype='int64'))
    #
    # # Construct matrix B (only needed for implementations)
    # mat_B = np.zeros((n, n), dtype='int64')
    # for i in np.arange(1, n, 2, dtype='int64'):
    #     mat_B[:n - i, i:] += np.eye(n - i, dtype='int64')
    # mat_2_inv_B = (p + 1) // 2 * mat_B

    #################################
    # Calculate (I - 2^{-1} B)^{-1} #
    #################################

    # Algorithm 1: Naive, ~n^4 multiplications
    # # Construct matrix (I - 2^{-1} B)^{-1} = I + 2^{-1} B + (2^{-1} B)^2 + ... + (2^{-1} B)^{n-1}
    # mat_I_min_2_inv_B_inv = np.eye(n, dtype='int64')
    # mat_2_inv_B_power = np.eye(n, dtype='int64')
    # for i in range(1, n):
    #     mat_2_inv_B_power = (mat_2_inv_B_power @ mat_2_inv_B) % p
    #     mat_I_min_2_inv_B_inv += mat_2_inv_B_power
    #     mat_I_min_2_inv_B_inv %= p

    # Algorithm 2: Clever multiplication, ~2 log_2(n) n^3 multiplications
    # # Construct matrix (I - 2^{-1} B)^{-1} = I + 2^{-1} B + (2^{-1} B)^2 + ...
    # # Optimize by noticing                 = (I + 2^{-1} B) (I + (2^{-1} B)^2) (I + (2^{-1} B)^4) (I + (2^{-1} B)^8) ...
    # # Hence only around log_2(n) matrix multiplications => O(n^3 log(n)) runtime
    # list_mats_power = [mat_2_inv_B]
    # power = 2
    # while power <= n:
    #     list_mats_power.append((list_mats_power[-1] @ list_mats_power[-1]) % p)
    #     power *= 2
    #
    # while len(list_mats_power) > 1:
    #     mat_last = list_mats_power.pop()
    #     list_mats_power[-1] += list_mats_power[-1] @ mat_last + mat_last
    #     list_mats_power[-1] %= p
    #
    # mat_I_min_2_inv_B_inv = list_mats_power[0] + np.eye(n, dtype='int64')

    # Algorithm 3: Unit upper triangular inverted using Gaussian elimination, ~1/6 n^3 multiplications
    # # Since (I - 2^{-1} B) is unit upper triangular, taking its inverse by backwards Gaussian elimination is easy
    # # Matrix entries can be as large as around (pn)^2, so make sure dtype can hold integers of that size
    # mat_I_min_2_inv_B_inv = -mat_2_inv_B
    # for i in range(n - 1, 0, -1):
    #     mat_I_min_2_inv_B_inv[:i, i] = p - mat_I_min_2_inv_B_inv[:i, i]
    #     mat_I_min_2_inv_B_inv[:i, i + 1:] += (mat_I_min_2_inv_B_inv[:i, i:i + 1] @ mat_I_min_2_inv_B_inv[
    #                                                                                i:i + 1, i + 1:]) % p
    # mat_I_min_2_inv_B_inv %= p
    # mat_I_min_2_inv_B_inv += np.eye(n, dtype='int64')

    # Algorithm 4: Taking advantage that (I - 2^{-1} B)^{-1} is an upper triangular Toeplitz matrix, O(n^2) time
    # Upper triangular Toeplitz matrices are closed under matrix addition, multiplication, and inversion,
    # so we only need to keep track of the top row
    plus_2_inv = (p + 1) // 2
    row_I_min_2_inv_B_inv = np.zeros(n, dtype='int64')
    row_I_min_2_inv_B_inv[0] = 1
    for i in range(1, n):
        if i <= 2:
            row_I_min_2_inv_B_inv[i] = (plus_2_inv * row_I_min_2_inv_B_inv[i - 1]) % p
        else:
            row_I_min_2_inv_B_inv[i] = (plus_2_inv * row_I_min_2_inv_B_inv[i - 1] + row_I_min_2_inv_B_inv[i - 2]) % p

    ########################################################
    # Construct matrix A = J (I + B mat_I_min_2_inv_B_inv) #
    ########################################################

    # For algorithm 1, 2, 3: Naive matrix multiplication, ~2 n^3 multiplications
    # mat_A = mat_J @ (np.eye(n, dtype='int64') + mat_B @ mat_I_min_2_inv_B_inv)
    # mat_A %= p

    # For algorithm 4:
    # Use that J, I, B, and (I - 2^{-1} B)^{-1} are all upper diagonal-constant matrices, O(n^2) time
    row_A = np.zeros(n, dtype='int64')

    # Do B (I - 2_inv_B_inv)^{-1}
    row_A[1] = 1
    for i in range(1, n):
        if i <= 2:
            row_A[i] = row_I_min_2_inv_B_inv[i - 1]
        else:
            row_A[i] = (row_I_min_2_inv_B_inv[i - 1] + row_A[i - 2]) % p
    # Add I
    row_A[0] = 1

    # Left-multiply by J and create matrix object
    mat_A = np.zeros((n, n), dtype='int64')
    mat_A[0] = row_A
    mat_A[1, 1:] = p - row_A[:-1]
    for i in range(2, n):
        mat_A[i, i:] = mat_A[i - 2, i - 2:-2]

    return mat_A


def generate_depth_first_traversal(n: int) -> np.ndarray:
    """
    Generates an array containing node numbers of a binary tree traversed depth-first,
    with base node = 1 and tree containing nodes 1 up to and including n.
    """
    result = np.zeros(n, dtype=int)
    node = 1
    for i in range(n):
        result[i] = node
        if 2 * node > n:
            if node == n:
                node //= 2
            while node % 2 == 1:
                node = (node - 1) // 2
            node += 1
        else:
            node *= 2

    return result


def encode_diffusion_conlang(message: pd.DataFrame) -> pd.DataFrame:
    """
    Converts messages from tree representation into its syllable realization.

    :param message: a DataFrame containing the message you want to convert. Must have columns (for each row, Character or Index must be non-empty):
        Character: the Goptjaam-compatible Classical Chinese characters found in the original message.
        Index: the Goptjaam indices of the morphemes of the original message
        Case: the cases of the morphemes of the original message
    :return: a DataFrame with columns:
        Index: the sentence vector; the Goptjaam indices of the converted syllables
        Character: the initial Goptjaam character corresponding to each index
        Transcription: the syllables in Baxter–Sagart notation of Middle Chinese
        Orthography: the syllables in Sino-Dutch orthography
        IPA: the pronunciation of the syllables in Sino-Dutch
    """
    mc = MCSyllables()

    # For each row, check if data is valid, and collect Indices into list_index
    list_case = []
    list_index = []
    for i, row in message.iterrows():
        # Check if case is valid
        case = int(row['Case'])
        if case < 1:
            raise ValueError(f'Case {case} in row {i} should be a positive integer!')
        if case in list_case:
            raise ValueError(f'Case {case} in row {i} should not repeat!')
        list_case.append(case)

        # Check if index and character are valid. Possibilities:
        # 1. If index not provided:
        #   1.1. If character not provided, then error
        #   1.2. If character not in Character nor in Homophones, then error
        #   1.3. If character not in Character but in Homophones, then error listing out possible Characters
        #   1.4. If character in Character once, then good!
        #   1.5. If character in Character multiple times, then error listing out possible Indices
        # 2. If index provided, use it as Index, but also:
        #   2.1. If character not provided, then warning
        #   2.2. If character not in Character nor in Homophones, then warning
        #   2.3. If character not in Character but in Homophones, then warning listing out possible Characters
        #   2.4. If character in Character and matches provided index, then good!
        #   2.5. If character in Character but doesn't match provided index, then warning listing out possible Indices

        if pd.isna(row['Index']):
            # 1.1
            if pd.isna(row['Character']):
                raise ValueError(f'Row {i} has neither Index nor Character specified!')

            indices_character = mc.classical_chinese_to_indices_character(row['Character'])
            indices_homophones = mc.classical_chinese_to_indices_homophone(row['Character'])

            if not indices_character:
                # 1.2
                if not indices_homophones:
                    raise ValueError(f'Character {row["Character"]} not Goptjaam-compatible!')
                # 1.3
                else:
                    raise ValueError(f'Character {row["Character"]} not Goptjaam-compatible!\n'
                                     f'Possible variant characters:\n'
                                     f'{mc.subdf_from_indices(indices_homophones)[["Character", "Homophones", "Transcription"]]}')
            # 1.4
            elif len(indices_character) == 1:
                index = indices_character[0]
            # 1.5
            else:
                raise ValueError(f'Character {row["Character"]} corresponds to multiple indices!\n'
                                 f'Possible indices:\n'
                                 f'{mc.subdf_from_indices(indices_character)[["Character", "Transcription"]]}')
        else:
            # Validate index
            index = int(row['Index'])
            if not 0 <= index < mc.p_syllables:
                warnings.warn(f'Index {row["Index"]} in row {i} should be in interval [0, {mc.p_syllables})!')

            # 2.1
            if pd.isna(row['Character']):
                warnings.warn(f'Row {i} has no specified character! Using Index {index}')

            indices_character = mc.classical_chinese_to_indices_character(row['Character'])
            indices_homophones = mc.classical_chinese_to_indices_homophone(row['Character'])

            if not indices_character:
                # 2.2
                if not indices_homophones:
                    warnings.warn(f'Character {row["Character"]} not Goptjaam-compatible! Using Index {index}')
                # 2.3
                else:
                    warnings.warn(f'Character {row["Character"]} not Goptjaam-compatible! Using Index {index}\n'
                                  f'Possible variant characters:\n'
                                  f'{mc.subdf_from_indices(indices_homophones)[["Character", "Homophones", "Transcription"]]}')
            # 2.4
            elif index in indices_character:
                pass
            # 2.5
            else:
                warnings.warn(f'Character {row["Character"]} does not correspond to Index {index}!\n'
                              f'Possible indices:\n'
                              f'{mc.subdf_from_indices(indices_character)[["Character", "Transcription"]]}')

        list_index.append(index)

    # Make summary vector
    message['Case'] = message['Case'].astype(int)
    n = message['Case'].max()  # Largest case = size of matrix = length of output
    vector_summary = np.zeros(n, dtype=np.int32)
    vector_summary[message['Case'] - 1] = list_index
    print(f'Summary vector:\n{vector_summary}\n')

    # Construct conversion matrix
    mat_upper = generate_triangular_self_inverse_matrix(mc.p_syllables, n)

    vector_sentence = mat_upper.dot(-vector_summary) % mc.p_syllables
    vector_sentence = mat_upper.T.dot(vector_sentence) % mc.p_syllables
    print(f'Sentence vector:\n{vector_sentence}\n')

    # Map converted indices to characters
    characters_convert = [mc.index_to_character(index) for index in vector_sentence]
    transcription_convert = [mc.index_to_transcription(index) for index in vector_sentence]
    orthography_convert = [mc.index_to_orthography(index) for index in vector_sentence]
    ipa_convert = [mc.index_to_ipa(index) for index in vector_sentence]

    output = pd.DataFrame({
        'Index': vector_sentence,
        'Character': characters_convert,
        'Transcription': transcription_convert,
        'Orthography': orthography_convert,
        'IPA': ipa_convert
    })

    return output


def decode_diffusion_conlang(message: pd.DataFrame) -> pd.DataFrame:
    """
    Converts text from syllable realization into its tree representation.

    :param message: a DataFrame containing the message you want to convert. Must have columns (no empty entries):
        Character: the syllables written in Han characters
        Orthography: the syllables in the Dutch-based orthography
    :return: a DataFrame with columns:
        Character: possible Guangyun characters found in the tree representation
                   (due to homophones, may contain multiple characters)
        Index: the Guangyun indices of the morphemes of the message
        Case: the cases of the morphemes of the message
    """
    mc = MCSyllables()

    # Make sure all entries are filled
    if pd.isna(message).any().any():
        raise ValueError(f'No entry should be blank!')

    # Here onwards we assume message is well-formed
    n = len(message)

    # Convert to indices
    indices_sentence = message.apply(lambda row:
                                     mc.character_orthography_to_index(row['Character'], row['Orthography']), axis=1)
    vector_sentence = indices_sentence.to_numpy(dtype=int)
    print(f'Sentence vector:\n{vector_sentence}\n')

    # Transcription

    # Construct conversion matrix
    mat_upper = generate_triangular_self_inverse_matrix(mc.p_syllables, n)

    # Convert
    vector_summary = mat_upper.T.dot(-vector_sentence) % mc.p_syllables
    vector_summary = mat_upper.dot(vector_summary) % mc.p_syllables
    print(f'Summary vector:\n{vector_summary}\n')

    # Use depth-first traversal because I think it makes it easier to understand the utterance just by looking
    cases_out = generate_depth_first_traversal(n)
    indices_out = vector_summary[cases_out - 1]

    # Remove 0-indices
    cases_out = cases_out[indices_out != 0]
    indices_out = indices_out[indices_out != 0]

    # Convert from indices to characters
    characters_out = [mc.index_to_character(index) for index in indices_out]

    output = pd.DataFrame({
        'Character': characters_out,
        'Index': indices_out,
        'Case': cases_out
    })

    return output


if __name__ == '__main__':
    # To translate:
    # According to all known laws of aviation, there is no way a bee should be able to fly.
    # Its wings are too small to get its fat little body off the ground.
    # The bee, of course, flies anyway because bees don't care what humans think is impossible.

    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='the (.csv) file to convert')
    parser.add_argument('-o', '--output', help='filepath to output to')
    args = parser.parse_args()

    # Get file
    message = pd.read_csv(args.file, na_values=[''], keep_default_na=False)
    print(f'\nInput:\n{message}\n')

    cols_encode = ['Character', 'Index', 'Case']
    cols_decode = ['Character', 'Orthography']
    if all(col in message.columns for col in cols_encode):
        message_out = encode_diffusion_conlang(message)
        if args.output is not None:
            filename_output = args.output
        else:
            filename_output = f'{args.file.removesuffix(".csv")}_encoded.csv'
        message_out.to_csv(filename_output, index=False)
    elif all(col in message.columns for col in cols_decode):
        message_out = decode_diffusion_conlang(message)
        if args.output is not None:
            filename_output = args.output
        else:
            filename_output = f'{args.file.removesuffix(".csv")}_decoded.csv'
        message_out.to_csv(filename_output, index=False)
    else:
        raise ValueError(f'File {args.file} is not a valid .csv file!\n'
                         f'If converting to Goptjaam, it must have columns Character, Index, Case.\n'
                         f'If converting from Goptjaam, it must have columns Character, Orthography.\n')

    print(f'Output:\n{message_out}\n')
    print(f'Saved as: {filename_output}\n')
