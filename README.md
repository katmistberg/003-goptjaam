# Goptjaam

[![MIT License][mit-shield]][mit] [![CC BY-NC 4.0][cc-by-nc-4.0-shield]][cc-by-nc-4.0]

The git repo for [Goptjaam](https://www.youtube.com/watch?v=ze5i_e_ryTk), a cursed conlang made by [Kat Mistberg 霧山豸砂](https://www.youtube.com/@KatMistberg) for [Agma Schwa](https://www.youtube.com/@AgmaSchwa)'s [Cursed Conlang Circus 2](https://www.youtube.com/watch?v=_2fhHUuMxcA) contest. The conlang includes cursed features such as:

- Human concepts are diffused across the entire utterance
- Infinitely many grammatical cases
- Constructed using linear algebra
- Dutch-based phonology

The language is described in [this video (34 minutes)](https://www.youtube.com/watch?v=ze5i_e_ryTk).

This repo contains:

- `goptjaam.py`: the Python script used to translate between (concept, case) pairs and Goptjaam. Run this to translate texts to/from Goptjaam.
- `generate_syllables_goptjaam.py`: the Python script used to generate the `data/syllables-goptjaam.csv` file. You do not need to run this.
- `derivation_A.md`: the derivation for the formula for the leading self-inverse matrix $A$ as mentioned in the video
- `texts`: contains the example texts used in the video
- `data`, `guangyun0704`: contain data files that `goptjaam.py` needs in order to do the translation
- `environment.yml`: lists the libraries you need in order to run the Python script



## Installation

If you are familiar with conda/mamba and Python, here's the summary:

Setup (replace `conda` with `mamba` if you're using mamba):
```bash
git clone <this repo>
cd 003-goptjaam
conda env create -n goptjaam --file environment.yml
```



### "I have no idea what those words mean"

[Conda](https://docs.conda.io/en/latest/) is a program used to install the programming language Python and manage the installation of Python libraries (code that other people wrote that I'm using in `goptjaam.py`). [Mamba](https://mamba.readthedocs.io/en/latest/mamba-installation.html#mamba-install) is a rewritten version of conda that is a lot faster (I highly recommend it!). The above code is used to setup the conda environment with the correct version of the libraries to run `goptjaam.py`. If you want to run the script, the steps are:
1. install [mamba](https://mamba.readthedocs.io/en/latest/mamba-installation.html#mamba-install)
2. download/clone this repo
3. run the setup in the terminal/console

Ask ChatGPT or your favorite web search engine if you're stuck.



## Running the script

Run `goptjaam.py` (replace `conda` with `mamba` if you're using mamba):
```bash
conda activate goptjaam
python goptjaam.py texts/opening_bee_movie.csv
conda deactivate
```

Files to be translated are stored as `.csv` files with a specific format. `goptjaam.py` automatically detects whether to translate into or out of Goptjaam based on what columns are present in the `.csv` file. If you want to translate your own texts, take a look at the examples in `texts`.



## FAQ

- "When I run `goptjaam.py`, I get a warning like this! Help!"
```
UserWarning: Character 幹 has no index 0.0 (possible indices are [2639])!
```
or
```
UserWarning: Character 食 has ambiguous readings [2264, 3699]!
```

> Some characters have multiple readings in the 廣韻. So we tell the `goptjaam.py` which reading to use by specifying it in the `Index` column of the text `.csv` file.
>
> The 1st warning appears when the index provided in the `Index` column is not a reading of the character; the provided index is still used in the translation. If you are using this index on purpose, then just ignore the warning.
>
> The 2nd warning appears when a character has multiple readings but no index was provided to disambiguate them. In this case, use Wiktionary to figure out which pronunciation you want to use, then use the file `data/syllables-goptjaam.csv` to figure out which index number the pronunciation corresponds to, and add the index in the text `.csv` file.



## Contributing

No new features are planned for `goptjaam.py`, since I want it to stay basically the same as the version used in the video. There still some TODOs in `goptjaam.py` on checking if the data is well-formed, which I may implement in the future (and other lies I tell myself). 

If you want to implement these TODOs for me, that would be of great help! If you find any issues/errors, feel free to open an issue and we'll figure it out.

If you find the derivation of $A$ to be unclear, or if there is something wrong with the derivation, open an issue and let me know.



## Acknowledgment
I would like to thank:
- [陳彭年](https://zh.wikipedia.org/zh-tw/%E9%99%B3%E5%BD%AD%E5%B9%B4), 邱雍, and everyone else who worked on it for compiling and editing the 大宋重修廣韻
- 有女同車, Biopolyhedron, and Zgheng for distilling the pronunciations of the 廣韻 into a machine-readable format
- [syimyuzya](https://github.com/syimyuzya) for converting it into the `.csv`-like file `Kuankhiunn0704-semicolon.txt` and keeping it available in [this Github repo](https://github.com/syimyuzya/guangyun0704).

Without all of you, I wouldn't have been able to convert Han characters into integers for Goptjaam (actually I probably could have found another way).



## License

Everything in this repo except for the `guangyun0704` directory and its contents is licensed under the [MIT License][mit].

The `guangyun0704` directory and its contents are licensed under the
[Creative Commons Attribution 4.0 International License][cc-by-nc-4.0], due to non-commercial limitations in [the license of the file `Kuankhiunn0704-semicolon.txt`](https://github.com/syimyuzya/guangyun0704#%E8%B2%AC%E6%AC%8A%E8%81%B2%E6%98%8E).

[cc-by-nc-4.0]: https://creativecommons.org/licenses/by-nc/4.0/
[cc-by-nc-4.0-image]: https://i.creativecommons.org/l/by-nc/4.0/88x31.png
[cc-by-nc-4.0-shield]: https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg

[mit]: https://mit-license.org/
[mit-shield]: https://img.shields.io/badge/License-MIT%20License-lightgrey.svg
