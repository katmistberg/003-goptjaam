# `guangyun0704`

Contains files derived from `Khankhiunn0704-semicolon.txt`. These shouldn't be edited.

## Files

- [`Khankhiunn0704-semicolon.txt`](https://github.com/syimyuzya/guangyun0704): MC syllable description, polyhedron transcription, and 有女 transcription of each 廣韻 syllable (小韻) in 廣韻 order. See below for license details. Not made by me; originally compiled by 有女同車, Biopolyhedron, and Zgheng; made into a `.csv` format by [syimyuzya](https://github.com/syimyuzya).
- `syllables-goptjaam.csv`: file used by `goptjaam.py` to map between characters, syllables, and numbers. Automatically generated from `Khankhiunn0704-semicolon.txt` by the script `generate_syllables_goptjaam.py`.

## License

The `guangyun0704` directory and its contents are licensed under the
[Creative Commons Attribution 4.0 International License][cc-by-nc-4.0]. This basically means that you are allowed to share and adapt it, but you must attribute where you got the files from, and **commercial use is not permitted**.

[![CC BY 4.0][cc-by-nc-4.0-image]][cc-by-nc-4.0]

I would have liked to license everything under the MIT license, but the non-commercial limitation is due to [the license of the file `Kuankhiunn0704-semicolon.txt`](https://github.com/syimyuzya/guangyun0704#%E8%B2%AC%E6%AC%8A%E8%81%B2%E6%98%8E), which states:
> 本版與原版相同，使用者允許複製、再發佈和修改其內容。切勿將其用於一切贏利目的。整理者對資料的正確性不提供任何擔保，使用者風險自負。

English (translated by me):
> This version is the same as the original version. You are allowed to copy, redistribute, and change its contents. Do not use it for any for-profit purposes. The creators provide no warranty on the accuracy of the contents. Use at your own risk.

[cc-by-nc-4.0]: https://creativecommons.org/licenses/by-nc/4.0/
[cc-by-nc-4.0-image]: https://i.creativecommons.org/l/by-nc/4.0/88x31.png
