import numpy as np
import pandas as pd
import argparse


def test_well_formedness(string_tree):
    # Test well-formedness of string_tree. Error iff not well-formed
    curly_closed = '}'
    stack = ['T']  # Possible stack states: T T, , T) ) T,} ,}
    for i, char in enumerate(string_tree):
        if stack[-1] == 'T':
            if char == '(':
                stack.pop()
                stack += ['T)', 'T,']
            elif char == '{':
                stack.pop()
                stack += ['T,}']
            elif char in [',', ')', '}']:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, cannot be , ) nor {curly_closed}')
        elif stack[-1] == 'T,':
            if char == '(':
                stack.pop()
                stack += [',', 'T)', 'T,']
            elif char == '{':
                stack.pop()
                stack += [',', 'T,}']
            elif char == ',':
                stack.pop()
            elif char in [')', '}']:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, cannot be ) nor {curly_closed}')
        elif stack[-1] == ',':
            if char == ',':
                stack.pop()
            else:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, should be ,')
        elif stack[-1] == 'T)':
            if char == '(':
                stack.pop()
                stack += [')', 'T)', 'T,']
            elif char == '{':
                stack.pop()
                stack += [')', 'T,}']
            elif char == ')':
                stack.pop()
            elif char in [',', '}']:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, cannot be , nor {curly_closed}')
        elif stack[-1] == ')':
            if char == ')':
                stack.pop()
            else:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, should be )')
        elif stack[-1] == 'T,}':
            if char == '(':
                stack.pop()
                stack += [',}', 'T)', 'T,']
            elif char == '{':
                stack.pop()
                stack += [',}', 'T,}']
            elif char == '}':
                stack.pop()
            elif char in [')']:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, cannot be )')
        elif stack[-1] == ',}':
            if char == ',':
                stack.pop()
                stack += ['T,}']
            elif char == '}':
                stack.pop()
            else:
                raise ValueError(f'Invalid {char} at index {i}, state {stack[-1]}, should be , or {curly_closed}')
        print(i, char, stack)

    if not(not stack or stack == ['T']):
        raise ValueError(f'There are unclosed brackets in string {string_tree}')


class Node:
    def __init__(self, content: str, child_l, child_r):
        self.content = content

        if child_l is None:
            self.child_l = TreeEmpty()
        else:
            self.child_l = child_l

        if child_r is None:
            self.child_r = TreeEmpty()
        else:
            self.child_r = child_r

        self.is_terminal = child_l is None and child_r is None

        self.depth = max(self.child_l.depth, self.child_r.depth) + 1

    def __str__(self):
        if self.is_terminal:
            return self.content
        else:
            return f'{self.content}({self.child_l},{self.child_r})'

    def set_content(self, content):
        self.content = content

    def get_cases_tree(self, case_root=1) -> pd.DataFrame:
        if self.content:
            df_this = pd.DataFrame({'Character': self.content, 'Index': '', 'Case': case_root}, index=[0])
        else:
            df_this = pd.DataFrame()

        df_l = self.child_l.get_cases_tree(2 * case_root)
        df_r = self.child_r.get_cases_tree(2 * case_root + 1)
        return pd.concat([df_this, df_l, df_r])


class TreeEmpty:
    def __init__(self):
        self.depth = 0

    def get_cases_tree(self, case_root=1) -> pd.DataFrame:
        return pd.DataFrame()


def notation_tree_to_node(string_tree: str) -> Node:
    # Assume well-formedness

    # Case t(a,b)
    if string_tree and string_tree[-1] == ')':
        parent, _, children = string_tree[:-1].partition('(')

        # Split children
        counter_bracket = 0
        for i, char in enumerate(children):
            if char in ['(', '{']:
                counter_bracket += 1
            elif char in [')', '}']:
                counter_bracket -= 1
            elif char == ',' and counter_bracket == 0:
                i_comma = i
                break

        child_l, child_r = children[:i_comma], children[i_comma + 1:]

        return Node(parent, notation_tree_to_node(child_l), notation_tree_to_node(child_r))

    # Case t{a,b,c,…}
    elif string_tree and string_tree[-1] == '}':
        parent, _, children = string_tree[:-1].partition('{')

        # Split children
        counter_bracket = 0
        list_i_comma = []
        for i, char in enumerate(children):
            if char in ['(', '{']:
                counter_bracket += 1
            elif char in [')', '}']:
                counter_bracket -= 1
            elif char == ',' and counter_bracket == 0:
                list_i_comma.append(i)

        list_i_comma = [-1] + list_i_comma + [len(children)]
        list_children = []
        for i in range(len(list_i_comma) - 1):
            list_children.append(children[list_i_comma[i] + 1:list_i_comma[i + 1]])

        # Get children nodes
        list_nodes_children = [notation_tree_to_node(child) for child in list_children]
        print([child.depth for child in list_nodes_children])

        # Combine children nodes (greedy algorithm, iteratively combine neighboring nodes that give the shallowest tree)
        while len(list_nodes_children) > 1:
            list_depths_parents_possible = []
            for i in range(len(list_nodes_children) - 1):
                list_depths_parents_possible.append(max(list_nodes_children[i].depth, list_nodes_children[i+1].depth))
            i_argmin = np.argmin(list_depths_parents_possible)
            list_nodes_children = (list_nodes_children[:i_argmin] +
                                   [Node('', list_nodes_children[i_argmin], list_nodes_children[i_argmin + 1])] +
                                   list_nodes_children[i_argmin + 2:])

        list_nodes_children[0].set_content(parent)
        return list_nodes_children[0]

    # Terminal case
    else:
        return Node(string_tree, None, None)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='the file with the tree ')
    parser.add_argument('-o', '--output', help='filepath to output to')
    args = parser.parse_args()

    # Get string from file
    with open(args.file) as file:
        string_tree = file.read().replace('\n', '')

    test_well_formedness(string_tree)
    node_tree = notation_tree_to_node(string_tree)
    print(node_tree)
    print(node_tree.get_cases_tree())

    node_tree.get_cases_tree().to_csv(args.output, index=False)
