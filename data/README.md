# `data`

Contains files needed for `goptjaam.py` to run. These should not be edited.

## Files

- `sino_dutch_initials.csv`, `sino_dutch_finals.csv`: files mapping from MC syllable descriptions to Sino-Dutch initials and finals. Used by `goptjaam.py`.
