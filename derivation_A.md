# Derivation of the formula for $A$

At 21:22 of the video, I claimed that if we always choose $\mathbf{c} = \mathbf{0}$ and alternate $d$ between $+1$ and $-1$, then the leading self-inverse matrix $A$ can be expressed by the formula
$$ A = D + DB (I - 2^{-1} B)^{-1}, $$
where $D = \operatorname{diag}(1, -1, 1, -1, \dots)$, $I$ is the identity matrix, and $B$ is some matrix where $b_{ij} = 0$ if $i \ge j$ or $i + j$ is even.

Here is a derivation of this formula. I will assume that you are familiar with eigenvectors and diagonalization (eigenvector decomposition). I will also be sloppy with iteration indices because I'm lazy.

### The plan

The plan is that we diagonalize $A$ into $VDV^{-1}$, and then we can relate the eigenvector matrix $V$ with the $\mathbf{b}$ s in each iteration. We can obtain 2 equations relating the two, and with that we can solve for $A$ to get the formula.

### 1. Decomposing $A$

Notice that by $A$ is upper triangular since we always choose $\mathbf{c} = \mathbf{0}$. Also, we pick $d^{(m)} = (-1)^{m+1}$ for the diagonal. So we can decompose $A$ into
$$ A = D + U, $$
where $D = \operatorname{diag}(1, -1, 1, -1, \dots)$ and $U$ is the rest of the upper diagonal, which consists of exactly all the $\mathbf{b}$ s. Also, because $A$ is upper triangular with a diagonal with no zeros, it is also diagonalizable as
$$ A = V D V^{-1}, $$
where $V$ is an **upper diagonal** matrix of eigenvectors of $A$. I won't give a rigorous proof for this, but the gist is that each column $i$ of $A$ corresponds to column $i$ of $V$ with eigenvalue $d^{(i)}$, and you can prove it using mathematical induction.

So now we come up with 2 equations relating $U$ and $V$.

### 2. Using $V$ to construct $U$

Remember that each $\mathbf{b}^{(m)}$ must satisfy $A^{(m-1)} \mathbf{b}^{(m)} = -d^{(m)} \mathbf{b}^{(m)}$. This means that $\mathbf{b}^{(m)}$ is an eigenvector of $A^{(m-1)}$ with eigenvalue $-d^{(m)}$ (or $\mathbf{0}$). So $\mathbf{b}^{(m)}$ can be expressed as a linear combination of the columns of $V^{(m-1)}$.

While that's good, it would be better if we could express the $\mathbf{b}^{(m)}$ from every iteration $m$ in terms of the columns of the same $V$ (the $V$ of the final iteration). And actually we can! Because $V$ is upper diagonal, if $b^{(m)} = V^{(m-1)} y_m$ for some vector $y_m$, then $u_m = V \begin{bmatrix}y_m \\ 0 \\ \vdots\end{bmatrix}$, where $u_m$ is the $m$-th column of $U$ (0s are padded onto $y_m$ to match the width of $V$). On both sides, we're only adding 0s to the end of the vector, so both sides of the equation stay equal. (Take your time to think this through. Perhaps drawing what $b^{(m)}$, $V$, and $U$ look like would help.)

What this is saying is that for every $m$, the $m$-th column of $U$ is a linear combination of columns of $V$. But putting all the columns together, this then means that
$$ (\forall m: u_m = V \begin{bmatrix}y_m \\ 0 \\ \vdots\end{bmatrix}) \implies U = VB $$
where the $m$-th column of $B$ is $\begin{bmatrix}y_m \\ 0 \\ \vdots\end{bmatrix}$ !

There are many entries of $B$ that must be 0. All entries on or below the diagonal of $B$ must be 0, because it inherited the 0s from the 0s padded onto $y_m$. As for above the diagonal, remember that $A^{(m-1)} \mathbf{b}^{(m)} = -d^{(m)} \mathbf{b}^{(m)}$: the eigenvector of $\mathbf{b}^{(m)}$ must be $-d^{(m)}$. Since $d^{(m)}$ alternates, (after some careful thinking through, we can conclude that) there must be a checkerboard pattern of 0s above the diagonal of $B$. More precisely, for entry $b_{im}$ of $B$, if $i$ and $m$ are an odd number apart, then $d^{(i)} = -d^{(m)}$, so the $i$-th column of $V$ can contribute to $\mathbf{b}^{(m)}$, and so $b_{im}$ can be any integer; otherwise, $b_{im}$ must be 0.

So now we can use $V$ to construct $U$ with $U = VB$.

### 3. Using $U$ to construct $V$

Our goal in this section is, at iteration $m$, to expand $V^{(m-1)}$ to $V^{(m)}$ by filling out the $m$-th column of $V^{(m)}$. In other words, we want to construct an eigenvector of $A^{(m)}$ associated with the $m$-th column of $A^{(m)}$.

First, notice that the $m$-th column of $A^{(m)}$ is already almost an eigenvector with eigenvalue $d^{(m)}$:
$$ A^{(m)}
\begin{bmatrix}
    \mathbf{b}^{(m)} \\
    d^{(m)}
\end{bmatrix} =
\begin{bmatrix}
    A^{(m-1)} \mathbf{b}^{(m)} + d^{(m)} \mathbf{b}^{(m)} \\
    1
\end{bmatrix} =
\begin{bmatrix}
    A^{(m-1)} \mathbf{b}^{(m)} \\
    0
\end{bmatrix} + d^{(m)}
\begin{bmatrix}
    \mathbf{b}^{(m)} \\
    d^{(m)}
\end{bmatrix}. $$
But not quite. So instead, we try the vector $\begin{bmatrix}
    k\mathbf{b}^{(m)} \\
    d^{(m)}
\end{bmatrix}$, where $k$ is some yet-unknown constant:
$$ A^{(m)}
\begin{bmatrix}
    k\mathbf{b}^{(m)} \\
    d^{(m)}
\end{bmatrix} =
\begin{bmatrix}
    kA^{(m-1)} \mathbf{b}^{(m)} + d^{(m)} \mathbf{b}^{(m)} \\
    1
\end{bmatrix} = d^{(m)}
\begin{bmatrix}
    k d^{(m)} A^{(m-1)} \mathbf{b}^{(m)} + \mathbf{b}^{(m)} \\
    d^{(m)}
\end{bmatrix}. $$
Looks promising. Let's see if we can set the value of $k$ such that the vector is an eigenvector. We want this equation to hold:
$$ k d^{(m)} A^{(m-1)} \mathbf{b}^{(m)} + \mathbf{b}^{(m)} = k \mathbf{b}^{(m)} $$
Remember (again) that $A^{(m-1)} \mathbf{b}^{(m)} = -d^{(m)} \mathbf{b}^{(m)}$. So the equation becomes
$$ -k d^{(m)} d^{(m)} \mathbf{b}^{(m)} + \mathbf{b}^{(m)} = k \mathbf{b}^{(m)}. $$
We can simplify this a bit. Since $d^{(m)} = \pm 1$, we have that $d^{(m)} d^{(m)} = 1$. Also, each term is $\mathbf{b}^{(m)}$ multiplied by a constant, so we can cancel the $\mathbf{b}^{(m)}$ s. So we get
$$ \begin{align*}
&& -k + 1 &= k \\
&\implies & 2k &= 1 \\
&\implies & k &= 2^{-1}.
\end{align*} $$
Awesome! Note that I have written it as $2^{-1}$ instead of $\frac{1}{2}$ because we're working with integers modulo $p$ here, and the notation $\frac{1}{2}$ would suggest $0.5$, which is not what we want.

So this means that we can set $m$-th column of $V^{(m)}$ to be $\begin{bmatrix}
    2^{-1} \mathbf{b}^{(m)} \\
    d^{(m)}
\end{bmatrix}$. Writing it in terms of $U^{(m)}$, equivalently, the part of $V^{(m)}$ above the diagonal is $2^{-1} U^{(m)}$, and the diagonal of $V^{(m)}$ is $D^{(m)}$. Or perhaps more simply,
$$ V = D + 2^{-1} U. $$

### 4. Combining the equations

So now we have the 2 equations:
$$ U = VB \quad, \quad V = D + 2^{-1} U. $$
Substituting the $V$ of the 2nd equation into the 1st equation, we can then solve for $U$:
$$
\begin{align*}
    U &= (D + 2^{-1} U) B \\
    U &= DB + 2^{-1} UB \\
    U - 2^{-1} UB &= DB \\
    U (I - 2^{-1} B) &= DB \\
    U &= DB (I - 2^{-1} B)^{-1}
\end{align*}
$$
Then since $A = D + U$, we get the formula
$$ A = D + DB (I - 2^{-1} B)^{-1}. $$
And we're done! $\;\square$


### Some comments

You might be wondering how we know that $I - 2^{-1} B$ is invertible (we used it when solving for $U$ at the end). We know it because of a neat trick. You've probably learned at school at some point that
$$ \frac{1}{1-x} = 1 + x + x^2 + x^3 + \dots, $$
if $|x| < 1$ (equivalently, if $\lim_{m \to \infty} x^m = 0$). We can do basically the same thing with matrices:
$$ (I - X)^{-1} = 1 + X + X^2 + X^3 + \dots $$
if $\lim_{m \to \infty} X^m = 0$.

So if $\lim_{m \to \infty} 2^{-1} B^m = 0$, then $I - 2^{-1} B$ would have an inverse, namely
$$ (I - 2^{-1} B)^{-1} = 1 + (2^{-1} B) + (2^{-1} B)^2 + (2^{-1} B)^3 + \dots. $$
And in fact, it holds: $B^m = 0$ if $m \ge n$, where $n \times n$ is the size of the matrix $B$ (as well as $A$, $V$, etc.). We can see this because $B$ is 0 on and below the main diagonal, which means that $B^2$ is 0 on and below the diagonal 1 above the main diagonal, which means that $B^3$ is 0 on and below the diagonal 2 above the main diagonal, etc. until $B^n$ which is just 0 everywhere. Hence
$$ (I - 2^{-1} B)^{-1} = 1 + (2^{-1} B) + (2^{-1} B)^2 + \dots + (2^{-1} B)^{n-1}. $$
This also gives us a neat way of calculating $(I - 2^{-1} B)^{-1}$ over integers modulo $p$. General-purpose math ibraries such as NumPy can usually only calculate inverses over the real and complex numbers, so in `goptjaam.py`, I use this formula instead. Also, $2^{-1}$ is just simply $\frac{p+1}{2}$ since $p$ is prime and so is odd (we are not doing $p = 2$ anyways). In `goptjaam.py`, this is implemented in the function `generate_triangular_self_inverse_matrix`.